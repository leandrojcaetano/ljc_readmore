<?php
/**
 * Plugin Name: Read More
 * Plugin URI: https://leandrocaetano.com.br
 * Description: Acrescenta botao Read more
 * Version: 1.0.1
 * Author:Leandro Caetano
 * Author URI: https://leandrocaetano.com.br
 * License: GPLv2 or later
 */
 
// Inicio Bloco - CONTINE LENDO

// Incluir um o CSS que se encontra na mesma
// pasta do plugin.
function ljc_enqueue_styles() {
   $dir = plugin_dir_path(__FILE__);
	wp_enqueue_style('readnore', $dir . '/style.css', array(), 'all');
}
add_action( 'wp_enqueue_scripts', 'ljc_enqueue_styles' );



// Inserir JS - CONTINE LENDO
function ljc_wpb_hook_javascript() {
    ?>
<script type='text/javascript'>

window.onload = function () {
    var mover = document.getElementById("read_more");
	
    mover.addEventListener("click", function (event) {
    var content = document.getElementById("extra_content");
  	var btn = document.getElementById("readmore");
  	content.classList.toggle('show');
	btn.classList.toggle('disappear');
		/*
		 * Here there is the possibility to show / hide text by clicking on the button. Comment the top line and uncomment this.
		 * 
		/* if (content.classList.contains("show")) {
      		btn.innerHTML = "........";
  		} else {
      		btn.innerHTML = "Show More";
	  	}*/
    });
};
	
</script>
    <?php
}
add_action('wp_head', 'ljc_wpb_hook_javascript');


/*
 * 
 * Insert button (READ MORE)
 * 
 */

//Insert button (READ MORE) after third paragraph of single post content.
add_filter( 'the_content', 'ljc_prefix_insert_post_btn' );
function ljc_prefix_insert_post_btn( $content ) {
	$ad_code = '<div id="readmore" class="readmore"> <button id="read_more" class="read_more">Continue Lendo</button> </div> <div id="extra_content" class="extra_content">';
	if ( is_single() && ! is_admin() ) {
		return ljc_prefix_insert_after_paragraph( $ad_code, 3, $content );
	}
return $content;
}
 

// Parent Function that makes the magic happen
function ljc_prefix_insert_after_paragraph( $insertion, $paragraph_id, $content ) {
	$closing_p = '</p>';
	$paragraphs = explode( $closing_p, $content );
	foreach ($paragraphs as $index => $paragraph) {

		if ( trim( $paragraph ) ) {
			$paragraphs[$index] .= $closing_p;
		}

		if ( $paragraph_id == $index + 1 ) {
			$paragraphs[$index] .= $insertion;
		}
	}
	
	return implode( '', $paragraphs );
}


/*
 * 
 * End of Insert button (READ MORE) 
 * 
 */


/*
 * 
 * Insert End of hidden content 
 * 
 */


function ljc_auto_insert_after_post($content){
if (is_single()) {
$content .= '</div> <!-- End of hidden content -->';
}
return $content;
}
add_filter('the_content', 'ljc_auto_insert_after_post');


// Fim Bloco - CONTINUE LENDO